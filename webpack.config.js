const path = require("path");

module.exports = {
    target: "node",
    entry: {
        server: "./main.ts"
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "pokemon-api.js"
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: "/node_modules/"
            }
        ]
    },
    resolve: {
        extensions: [".ts", ".js"]
    }
}