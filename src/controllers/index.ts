export { PokemonApiController } from "./pokemon/pokemon-controller";
export { TypeApiController } from "./type/type-controller"; 
export { MoveApiController } from "./move/move-controller";
export { AbilityApiController } from "./ability/ability-controller";
export { ItemApiController } from "./item/item-controller";