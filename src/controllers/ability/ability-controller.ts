import { Request, Response } from "express";

import { AbilityRepository } from "./../../repositories";
import { Ability } from "./../../database/models";

export class AbilityApiController {

    async getAllAbilities(req: Request, res: Response) {
        const repository = new AbilityRepository();

        const docs = await repository.getAll();

        if (!docs) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            abilities: docs
        });
    }

    async getAbility(req: Request, res: Response) {
        const repository = new AbilityRepository();

        const doc = await repository.get(req.params.name);

        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            ability: doc
        });
    }

    async addAbility(req: Request, res: Response) {
        const repository = new AbilityRepository();

        try {
            const doc = await repository.insert(new Ability(req.body));
            
            if (!doc) {
                return res.status(404).send({
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                ability: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async updateAbility(req: Request, res: Response) {
        const repository = new AbilityRepository();

        try {
            const doc = await repository.update(req.params.name, req.body);

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                ability: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async deleteAbility(req: Request, res: Response) {
        const repository = new AbilityRepository();

        const doc = await repository.delete(req.params.name);
        
        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            ability: doc
        });
    }

}