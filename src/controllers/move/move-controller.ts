import { Request, Response } from "express";

import { MoveRepository } from "./../../repositories";
import { Move} from "./../../database/models";

export class MoveApiController {

    async getAllMoves(req: Request, res: Response) {
        const repository = new MoveRepository();

        const docs = await repository.getAll();

        if (!docs) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            moves: docs
        });
    }

    async getMove(req: Request, res: Response) {
        const repository = new MoveRepository();

        const doc = await repository.get(req.params.name);

        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            move: doc
        });
    }

    async addMove(req: Request, res: Response) {
        const repository = new MoveRepository();

        try {
            const doc = await repository.insert(new Move(req.body));
            
            if (!doc) {
                return res.status(404).send({
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                move: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async updateMove(req: Request, res: Response) {
        const repository = new MoveRepository();

        try {
            const doc = await repository.update(req.params.name, req.body);

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                move: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async deleteMove(req: Request, res: Response) {
        const repository = new MoveRepository();

        const doc = await repository.delete(req.params.name);
        
        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            move: doc
        });
    }

}