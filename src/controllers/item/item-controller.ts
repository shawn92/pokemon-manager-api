import { Request, Response } from "express";

import { ItemRepository } from "./../../repositories";
import { Item } from "./../../database/models";

export class ItemApiController {

    async getAllItems(req: Request, res: Response) {
        const repository = new ItemRepository();

        const docs = await repository.getAll();

        if (!docs) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            items: docs
        });
    }

    async getItem(req: Request, res: Response) {
        const repository = new ItemRepository();

        const doc = await repository.get(req.params.name);

        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            item: doc
        });
    }

    async addItem(req: Request, res: Response) {
        const repository = new ItemRepository();

        try {
            const doc = await repository.insert(new Item(req.body));
            
            if (!doc) {
                return res.status(404).send({
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                item: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async updateItem(req: Request, res: Response) {
        const repository = new ItemRepository();

        try {
            const doc = await repository.update(req.params.name, req.body);

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                item: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async deleteItem(req: Request, res: Response) {
        const repository = new ItemRepository();

        const doc = await repository.delete(req.params.name);
        
        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            item: doc
        });
    }

}