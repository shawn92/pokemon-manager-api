import { json } from "body-parser";
import { Request, Response } from "express";

export class UserApiController {
    constructor() {}

    getAllUsers(req: Request, res: Response) {
        res.send("NOT IMPLEMENTED: GET");
    }

    getUser(req: Request, res: Response) {
        res.send("NOT IMPLEMENTED: GET");
    }

    addUser(req: Request, res: Response) {
        res.send("NOT IMPLEMENTED: GET");
    }

    updateUser(req: Request, res: Response) {
        res.send("NOT IMPLEMENTED: GET");
    }

    loginUser(req: Request, res: Response) {
        res.send("NOT IMPLEMENTED: GET");
    }

    logoutUser(req: Request, res: Response) {
        res.send("NOT IMPLEMENTED: GET");
    }

    deleteUser(req: Request, res: Response) {
        res.send("NOT IMPLEMENTED: GET");
    }
}