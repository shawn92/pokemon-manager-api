import { Request, Response } from "express";

import { TypeRepository } from "./../../repositories";
import { Type } from "./../../database/models";

export class TypeApiController {

    async getAllTypes(req: Request, res: Response) {
        const repository = new TypeRepository();

        const docs = await repository.getAll();

        if (!docs) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            types: docs
        });
    }

    async getType(req: Request, res: Response) {
        const repository = new TypeRepository();

        const doc = await repository.get(req.params.name);

        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            type: doc
        });
    }

    async addType(req: Request, res: Response) {
        const repository = new TypeRepository();

        try {
            const doc = await repository.insert(new Type(req.body));

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                type: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async updateType(req: Request, res: Response) {
        const repository = new TypeRepository();

        try {
            const doc = await repository.update(req.params.name, req.body);

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                type: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async deleteType(req: Request, res: Response) {
        const repository = new TypeRepository();

        const doc = await repository.delete(req.params.name);
        
        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            type: doc
        });
    }

}