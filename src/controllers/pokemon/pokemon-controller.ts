import { Request, Response } from "express";

import { PokemonRepository } from "./../../repositories";
import { Pokemon } from "./../../database/models";

export class PokemonApiController {

    async getAllPokemon(req: Request, res: Response) {
        const repository = new PokemonRepository();

        const docs = await repository.getAll(req.query);

        if (!docs) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            pokemon: docs
        });
    }

    async getPokemon(req: Request, res: Response) {
        const repository = new PokemonRepository();
        
        const doc = await repository.get(req.params.name);

        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            pokemon: doc
        });
    }

    async addPokemon(req: Request, res: Response) {
        const repository = new PokemonRepository();

        try {
            const doc = await repository.insert(new Pokemon(req.body));

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                pokemon: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async updatePokemon(req: Request, res: Response) {
        const repository = new PokemonRepository();

        try {
            const doc = await repository.update(req.params.name, req.body);

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                pokemon: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async deletePokemon(req: Request, res: Response) {
        const repository = new PokemonRepository();

        const doc = await repository.delete(req.params.name);
        
        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            pokemon: doc
        });
    }

}