export * from "./repository";
export * from "./pokemon/pokemon-repository";
export * from "./type/type-repository";
export * from "./move/move-repository";
export * from "./ability/ability-repository";
export * from "./item/item-repository";