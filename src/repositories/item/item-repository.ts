import * as mongoose from "mongoose";

import { Repository } from "./../repository";
import { IItem, Item } from "./../../database/models";

export class ItemRepository extends Repository<IItem> {
    constructor() {
        super(Item);
    }
}