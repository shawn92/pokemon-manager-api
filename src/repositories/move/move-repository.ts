import * as mongoose from "mongoose";

import { Repository } from "./../repository";
import { IMove, Move } from "./../../database/models";

export class MoveRepository extends Repository<IMove> {
    constructor() {
        super(Move);
    }
}