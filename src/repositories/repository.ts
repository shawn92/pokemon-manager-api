import * as mongoose from "mongoose";

export interface IRepository<T> {
    getAll(): Promise<mongoose.Document[]>;
    get(key: string): Promise<mongoose.Document>;
    insert(document: T): Promise<mongoose.Document>;
    update(key: string, body: {}): Promise<mongoose.Document>;
    delete(keys: string): Promise<mongoose.Document>;
};

export abstract class Repository<T extends mongoose.Document> implements IRepository<T> {
    public collection: mongoose.Model<mongoose.Document>;

    constructor(collection: mongoose.Model<mongoose.Document>) {
        this.collection = collection;
    }
    
    async getAll(): Promise<mongoose.Document[]> {
        return await this.collection.find();
    };

    async get(key: string): Promise<mongoose.Document> {
        return await this.collection.findOne({name: key});
    };

    async insert(document: T): Promise<mongoose.Document> {
        return await document.save();
    }

    async update(key: string, body: T): Promise<mongoose.Document> {
        return await this.collection.findOneAndUpdate({name: key},
            { $set: body },
            { runValidators: true, new: true }
        );
    }

    async delete(key: string): Promise<mongoose.Document> {
        return await this.collection.findOneAndRemove({name: key});
    }
}