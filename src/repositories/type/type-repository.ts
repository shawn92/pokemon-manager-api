import * as mongoose from "mongoose";

import { Repository } from "./../repository";
import { IType, Type } from "./../../database/models";

export class TypeRepository extends Repository<IType> {
    constructor() {
        super(Type);
    }
}