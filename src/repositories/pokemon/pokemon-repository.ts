import * as mongoose from "mongoose";
import * as _ from "lodash";

import { Request } from "express";

import { Repository } from "./../repository";
import { 
    IPokemon, 
    Pokemon, 
    PokemonQuery, 
    IType, 
    IMove
} from "./../../database/models";

export class PokemonRepository extends Repository<IPokemon> {
    constructor() {
        super(Pokemon);
    }

    async getAll(key?: PokemonQuery): Promise<mongoose.Document[]> {
        const docs = await this.collection.find()
            .populate("types.primary")
            .populate("types.secondary")
            .populate("abilities.primary")
            .populate("abilities.secondary")
            .populate("abilities.hidden")
            .populate("moves.learned.move")
            .populate("moves.egg.move")
            .populate("moves.tm")
            .exec();

        return docs.filter((doc: IPokemon) => {
            if (!!key.type) {
                return doc.types.primary.name === key.type || 
                    (!!doc.types.secondary && doc.types.secondary.name === key.type);
            }
            else if (!!key.ability) {
                console.log(key.ability);
                return doc.abilities.primary.name === key.ability || 
                (!!doc.abilities.secondary && doc.abilities.secondary.name === key.ability) ||
                (!!doc.abilities.hidden && doc.abilities.hidden.name === key.ability);
            }
            else if (!!key.move) {
                return _.find(_.map(doc.moves.learned, "move"), move => {
                    return move.name === key.move;
                }) ||
                _.find(doc.moves.tm, (move: IMove) => {
                    return move.name === key.move;
                }) ||
                _.find(_.map(doc.moves.egg, "move"), move => {
                    return move.name === key.move;
                })
            }
            return doc;
        });
    }

    async get(key: string): Promise<mongoose.Document> {
        return await this.collection.findOne({name: key})
            .populate("types.primary")
            .populate("types.secondary")
            .populate("abilities.primary")
            .populate("abilities.secondary")
            .populate("abilities.hidden")
            .populate("moves.learned.move")
            .populate("moves.egg.move")
            .populate("moves.tm")
            .exec();
    }
}