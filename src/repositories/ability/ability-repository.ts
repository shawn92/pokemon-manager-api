import * as mongoose from "mongoose";

import { Repository } from "./../repository";
import { IAbility, Ability } from "./../../database/models";

export class AbilityRepository extends Repository<IAbility> {
    constructor() {
        super(Ability);
    }
}