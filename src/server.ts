import * as express from "express";
import { json } from "body-parser";

import { 
    PokemonRouter,
    TypeRouter,
    MoveRouter,
    AbilityRouter,
    ItemRouter
} from "./routes";

export class App {
    public server: express.Application;

    constructor() {
        this.server = express();

        this.setMiddleware();
        this.configureRoutes();
    }

    private setMiddleware() {
        this.server.use(json());
    }

    private configureRoutes() {
        this.server.use("/api", [ 
            new PokemonRouter().routes,
            new TypeRouter().routes,
            new MoveRouter().routes,
            new AbilityRouter().routes,
            new ItemRouter().routes
        ]);
    }
}