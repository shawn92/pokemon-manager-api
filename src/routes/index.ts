export * from "./pokemon/pokemon-router";
export * from "./type/type-router";
export * from "./move/move-router";
export * from "./ability/ability-router";
export * from "./item/item-router";