import { Router } from "express";

import { MoveApiController } from "./../../controllers";

const router = Router();

export class MoveRouter {
    private controller: MoveApiController;
    
    constructor() {
        this.controller = new MoveApiController();
    }

    get routes() {
        const controller = this.controller;

        router.get("/moves", 
            controller.getAllMoves
        );

        router.get("/moves/:name", 
            controller.getMove
        );

        router.post("/moves", 
            controller.addMove
        );

        router.patch("/moves/:name", 
            controller.updateMove
        );

        router.delete("/moves/:name", 
            controller.deleteMove
        );

        return router;
    }
}