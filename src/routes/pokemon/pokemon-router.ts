import { Router } from "express";

import { PokemonApiController } from "./../../controllers";

const router = Router();

export class PokemonRouter {
    private controller: PokemonApiController;

    constructor() {
        this.controller = new PokemonApiController();
    }

    get routes() {
        const controller = this.controller;

        router.get("/pokemon", 
            controller.getAllPokemon
        );

        router.get("/pokemon/:name", 
            controller.getPokemon
        );

        router.post("/pokemon", 
            controller.addPokemon
        );

        router.patch("/pokemon/:name", 
            controller.updatePokemon
        );

        router.delete("/pokemon/:name", 
            controller.deletePokemon
        );

        return router;
    }
}