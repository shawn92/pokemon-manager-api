import { Router } from "express";

import { AbilityApiController } from "./../../controllers";

const router = Router();

export class AbilityRouter {
    private controller: AbilityApiController;
    
    constructor() {
        this.controller = new AbilityApiController();
    }

    get routes() {
        const controller = this.controller;

        router.get("/abilities", 
            controller.getAllAbilities
        );

        router.get("/abilities/:name", 
            controller.getAbility
        );

        router.post("/abilities", 
            controller.addAbility
        );

        router.patch("/abilities/:name", 
            controller.updateAbility
        );

        router.delete("/abilities/:name", 
            controller.deleteAbility
        );

        return router;
    }
}