import { Router } from "express";

import { ItemApiController } from "./../../controllers";

const router = Router();

export class ItemRouter {
    private controller: ItemApiController;
    
    constructor() {
        this.controller = new ItemApiController();
    }

    get routes() {
        const controller = this.controller;

        router.get("/items", 
            controller.getAllItems
        );

        router.get("/items/:name", 
            controller.getItem
        );

        router.post("/items", 
            controller.addItem
        );

        router.patch("/items/:name", 
            controller.updateItem
        );

        router.delete("/items/:name", 
            controller.deleteItem
        );

        return router;
    }
}