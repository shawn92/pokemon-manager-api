import { Router } from "express";

import { TypeApiController } from "./../../controllers";

const router = Router();

export class TypeRouter {
    private controller: TypeApiController;

    constructor() {
        this.controller = new TypeApiController();
    }

    get routes() {
        const controller = this.controller;

        router.get("/types", 
            controller.getAllTypes
        );

        router.get("/types/:name", 
            controller.getType
        );

        router.post("/types", 
            controller.addType
        );

        router.patch("/types/:name", 
            controller.updateType
        );

        router.delete("/types/:name", 
            controller.deleteType
        );

        return router;
    }
}