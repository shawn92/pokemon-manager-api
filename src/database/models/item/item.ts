import * as mongoose from "mongoose";

import { ITEMS } from "./items";

export interface IItem extends mongoose.Document {
    name: string;
    categories: string[],
    effect: string;
}

const ItemSchema = new mongoose.Schema({
    name: {
        type: String,
        maxlength: 20,
        required: true,
        unique: true
    },
    categories: {
        type: [String],
        enum: ITEMS,
        required: true
    },
    effect: {
        type: String,
        maxlength: 150,
        required: true
    }
});

export const Item = mongoose.model<IItem>("Item", ItemSchema, "items");