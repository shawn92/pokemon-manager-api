export const ITEMS = [
    "Berry",
    "Orb",
    "Drive",
    "Effort",
    "Evolution",
    "Experience",
    "Gem",
    "Incense",
    "Mega Stone",
    "Memory",
    "Plate",
    "Power",
    "Stat",
    "Z-Crystal"
];