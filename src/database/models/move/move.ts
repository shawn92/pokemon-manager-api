import * as mongoose from "mongoose";

import { TYPES } from "./../type/types";

export interface IMove extends mongoose.Document {
    name: string;
    type: string;
    category: string;
    power?: number;
    accuracy?: number;
    pp: number;
    effect: string;
    tm?: number;
};

const MoveSchema = new mongoose.Schema({
    name: {
        type: String,
        maxlength: 20,
        required: true,
        unique: true
    },
    type: {
        type: String,
        enum: TYPES,
        required: true
    },
    category: {
        type: String,
        enum: ["Physical", "Special", "Status"],
        required: true
    },
    power: {
        type: Number,
        min: 10,
        max: 200
    },
    accuracy: {
        type: Number,
        min: 30,
        max: 100
    },
    pp: {
        type: Number,
        min: 1,
        max: 40,
        required: true
    },
    effect: {
        type: String,
        maxlength: 200,
        required: true
    },
    tm: {
        type: Number
    }
});

MoveSchema.set("toJSON", {
    transform: (doc: IMove, obj: IMove) => {
        delete obj.__v;
        return obj;
    }
});

export const Move = mongoose.model<IMove>("Move", MoveSchema, "moves");