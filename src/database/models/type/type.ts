import * as mongoose from "mongoose";

import { IResistance, Resistance } from "./resistance";
import { TYPES } from "./types";

export interface IType extends mongoose.Document {
    name: string;
    resistance: IResistance
};

const TypeSchema = new mongoose.Schema({
    name: {
        type: String,
        enum: TYPES,
        required: true,
        unique: true
    },
    resistance: {
        type: Resistance,
        required: true
    }
});

TypeSchema.set("toJSON", {
    transform: (doc: IType, obj: IType) => {
        delete obj.__v;
        return obj;
    }
});

export const Type = mongoose.model<IType>("Type", TypeSchema, "types");