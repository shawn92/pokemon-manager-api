import * as mongoose from "mongoose";

export interface IResistance {
    fire: number;
    water: number;
    grass: number;
    electric: number;
    ground: number;
    rock: number;
    fighting: number;
    flying: number;
    poison: number;
    psychic: number;
    normal: number;
    ghost: number;
    bug: number;
    dark: number;
    steel: number;
    ice: number;
    dragon: number;
    fairy: number;
};

export const Resistance = new mongoose.Schema({
    fire: {
        type: Number,
        required: true
    },
    water: {
        type: Number,
        required: true
    },
    grass: {
        type: Number,
        required: true
    },
    electric: {
        type: Number,
        required: true
    },
    ground: {
        type: Number,
        required: true
    },
    rock: {
        type: Number,
        required: true
    },
    fighting: {
        type: Number,
        required: true
    },
    flying: {
        type: Number,
        required: true
    },
    poison: {
        type: Number,
        required: true
    },
    psychic: {
        type: Number,
        required: true
    },
    normal: {
        type: Number,
        required: true
    },
    ghost: {
        type: Number,
        required: true
    },
    bug: {
        type: Number,
        required: true
    },
    dark: {
        type: Number,
        required: true
    },
    steel: {
        type: Number,
        required: true
    },
    ice: {
        type: Number,
        required: true
    },
    dragon: {
        type: Number,
        required: true
    },
    fairy: {
        type: Number,
        required: true
    }
}, { _id: false });