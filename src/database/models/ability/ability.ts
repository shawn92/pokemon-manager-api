import * as mongoose from "mongoose";

export interface IAbility extends mongoose.Document {
    name: string;
    effect: string;
};

const AbilitySchema = new mongoose.Schema({
    name: {
        type: String, 
        maxlength: 20,
        required: true,
        unique: true
    },
    effect: {
        type: String,
        maxlength: 100,
        required: true
    }
});

AbilitySchema.set("toJSON", {
    transform: (doc: IAbility, obj: IAbility) => {
        delete obj.__v;
        return obj;
    }
});

export const Ability = mongoose.model<IAbility>("Ability", AbilitySchema, "abilities");