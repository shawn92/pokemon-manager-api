import * as mongoose from "mongoose";

import { IStats, Stats } from "./stats";
import { IMoveset, Moveset } from "./moveset";
import { IEvolution, Evolution } from "./evolution";

import { IType } from "./../../models/type/type";
import { IAbility } from "./../../models/ability/ability";

export interface PokemonQuery {
    type?: string;
    ability?: string;
    move?: string;
};

export interface IPokemon extends mongoose.Document {
    dex: number;
    name: string;
    types: {
        primary: IType;
        secondary?: IType;
    };
    category: string;
    weight: number;
    height: number;
    entry: string;
    abilities: {
        primary: IAbility;
        secondary?: IAbility;
        hidden?: IAbility;
    },
    stats: IStats;
    evolutions?: IEvolution[];
    moves: IMoveset
};

const PokemonSchema = new mongoose.Schema({
    dex: {
        type: Number,
        required: true,
        unique: true
    },
    name: {
        type: String,
        maxlength: 30,
        required: true,
        unique: true
    },
    types: {
        type: new mongoose.Schema({
            primary: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Type",
                required: true
            },
            secondary: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Type"
            }
        }, { _id: false }),
        required: true
    },
    category: {
        type: String,
        maxlength: 30,
        required: true
    },
    weight: {
        type: Number,
        required: true
    },
    height: {
        type: Number,
        required: true
    },
    entry: {
        type: String,
        maxlength: 200,
        required: true
    },
    abilities: {
        type: new mongoose.Schema({
            primary: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Ability",
                required: true
            },
            secondary: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Ability"
            },
            hidden: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Ability"
            }
        }, { _id: false }),
        required: true
    },
    stats: {
        type: Stats,
        required: true
    },
    evolutions: {
        type: [Evolution]
    },
    moves: {
        type: Moveset,
        required: true
    }
});

PokemonSchema.set("toJSON", {
    transform: (doc: IPokemon, obj: IPokemon) => {
        delete obj._id;
        delete obj.__v;
        return obj;
    }
});
export const Pokemon = mongoose.model<IPokemon>("Pokemon", PokemonSchema, "pokemon");