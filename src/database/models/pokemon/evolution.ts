import * as mongoose from "mongoose";

export interface IEvolution {
    name: string;
    order: number;
    cause: string;
};

export const Evolution = new mongoose.Schema({
    name: {
        type: String,
        maxlength: 20,
        required: true
    },
    order: {
        type: Number,
        required: true
    },
    cause: {
        type: String,
        maxlength: 40,
        required: true
    }
}, { _id: false });