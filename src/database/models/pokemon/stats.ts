import * as mongoose from "mongoose";

export interface IStats {
    hp: number;
    attack: number;
    defense: number;
    specialAttack: number;
    specialDefense: number;
    speed: number;
};

export const Stats = new mongoose.Schema({
    hp: { 
        type: Number,
        required: true
    },
    attack: { 
        type: Number,
        required: true
    },
    defense: { 
        type: Number,
        required: true
    },
    specialAttack: { 
        type: Number,
        required: true
    },
    specialDefense: { 
        type: Number,
        required: true
    },
    speed: { 
        type: Number,
        required: true
    }
}, { _id: false });