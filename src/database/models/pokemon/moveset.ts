import * as mongoose from "mongoose";

import { IMove } from "./../move/move";

export interface IMoveset {
    learned: {
        move: IMove;
        level: number;
    }[];
    tm?: IMove[],
    egg: {
        move: IMove;
        parent: string;
    }[];
};

export const Moveset = new mongoose.Schema({
    learned: {
        type: [
            new mongoose.Schema({
                move: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: "Move",
                    required: true
                },
                level: {
                    type: Number,
                    min: 1,
                    max: 100,
                    required: true
                }
            }, { _id: false })
        ],
        required: true
    },
    tm: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Move"
    }],
    egg: {
        type: [
            new mongoose.Schema({
                move: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: "Move",
                    required: true
                },
                parent: {
                    type: String,
                    required: true
                }
            }, { _id: false })
        ]
    }
}, { _id: false });