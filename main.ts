import * as mongoose from "mongoose";

import { App } from "./src/server";
import { DEV } from "./config";

const env = process.env.NODE_ENV || "dev";

if (env === "dev") {
     process.env.PORT = DEV.PORT;
     process.env.MONGODB_URI = DEV.MONGODB_URI;
}

mongoose.connect(process.env.MONGODB_URI);

const app = new App();

app.server.listen(process.env.PORT, () => {
    console.log(`Server running on port ${process.env.PORT}...`);
});